package model;

import java.io.Serializable;

public class User implements Serializable {

    private String mName;
    private int mScore = 0;
    private boolean mNewPlayer = true;

    public String getmName() {
        return mName;
    }

    public void setmName(String mName) {
        this.mName = mName;
    }

    public int getmScore() {
        return mScore;
    }

    public void setmScore(int mScore) {
        this.mScore = mScore;
    }

    public boolean ismNewPlayer() {
        return mNewPlayer;
    }

    public void setmNewPlayer(boolean mNewPlayer) {
        this.mNewPlayer = mNewPlayer;
    }
}
