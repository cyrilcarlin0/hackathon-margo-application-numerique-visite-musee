package hackathon.mmeo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import model.User;

public class MainActivity extends AppCompatActivity {

    private TextView mWelcomeText;
    private EditText mNameInput;
    private Button mStartButton;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        final User mCurrentUser = new User();

        mWelcomeText = (TextView) findViewById(R.id.activity_main_welcome_txt);
        mNameInput = (EditText) findViewById(R.id.activity_main_name_input);
        mStartButton = (Button) findViewById(R.id.activity_main_start_btn);

        mStartButton.setEnabled(false);

        mNameInput.addTextChangedListener(new TextWatcher() {

            @Override

            public void beforeTextChanged(CharSequence s, int start, int count, int after) {


            }


            @Override

            public void onTextChanged(CharSequence s, int start, int before, int count) {

                mStartButton.setEnabled(s.toString().trim().length() > 0);

            }


            @Override

            public void afterTextChanged(Editable s) {


            }
        });

        mStartButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mCurrentUser.setmName(mNameInput.getText().toString());
                Intent choixParcoursIntent = new Intent (MainActivity.this, ChoixParcours.class);
                choixParcoursIntent.putExtra("user", mCurrentUser);
                startActivity(choixParcoursIntent);
            }
        });
    }


}
