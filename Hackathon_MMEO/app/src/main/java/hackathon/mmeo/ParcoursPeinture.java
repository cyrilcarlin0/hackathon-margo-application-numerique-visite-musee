package hackathon.mmeo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import model.User;

public class ParcoursPeinture extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_parcours_peinture);

        TextView mTextWelcome = findViewById(R.id.activity_parcours_peinture_txt);
        final TextView mTextReady = findViewById(R.id.activity_parcours_peinture_ready_txt);
        final Button mYButton = findViewById(R.id.activity_parcours_peinture_y_btn);
        final Button mNButton = findViewById(R.id.activity_parcours_peinture_n_btn);
        final Button mReadyButton = findViewById(R.id.activity_parcours_peinture_ready_btn);
        final Button mNotReadyButton = findViewById(R.id.activity_parcours_peinture_notready_btn);
        final User user = (User) getIntent().getSerializableExtra("user");

        if(user.ismNewPlayer()) {
            mTextWelcome.setText("C'est parti " + user.getmName() + " ! Commençons ensemble le parcours peinture.");
            mTextReady.setVisibility(View.VISIBLE);
        } else {
            mTextWelcome.setText("Bravo " + user.getmName() + " ! Tu as fini une activité. Est-ce que c'était bien?");
            mYButton.setVisibility(View.VISIBLE);
            mNButton.setVisibility(View.VISIBLE);
        }

        View.OnClickListener listenerBtn = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                mYButton.setVisibility(View.GONE);
                mNButton.setVisibility(View.GONE);
                mTextReady.setVisibility(View.VISIBLE);
                mReadyButton.setVisibility(View.VISIBLE);
                mNotReadyButton.setVisibility(View.VISIBLE);
            }
        };

        mYButton.setOnClickListener(listenerBtn);
        mNButton.setOnClickListener(listenerBtn);

        mReadyButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent activityOnePeinture = new Intent(ParcoursPeinture.this, ActivityPuzzle.class);
                activityOnePeinture.putExtra("user", getIntent().getSerializableExtra("user"));
                user.setmNewPlayer(false);
                startActivity(activityOnePeinture);
            }
        });
    }
}
