package hackathon.mmeo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import model.User;

public class ChoixParcours extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_choix_parcours);

        final Intent intent = getIntent();
        User user = (User) intent.getSerializableExtra("user");

        TextView mGreetings = findViewById(R.id.activity_choix_parcours_greetings_txt);
        mGreetings.setText("Enchantée " + user.getmName() +" !\n Choisis le parcours que tu veux " +
                "suivre aujourd'hui");

        Button mBoutonParcoursPeinture = findViewById(R.id.activity_choix_parcours_btn1);

        mBoutonParcoursPeinture.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                    Intent peintureParcoursIntent = new Intent(ChoixParcours.this, ParcoursPeinture.class);
                    peintureParcoursIntent.putExtra("user", (User) intent.getSerializableExtra("user"));
                    startActivity(peintureParcoursIntent);
            }
        });
    }
}
