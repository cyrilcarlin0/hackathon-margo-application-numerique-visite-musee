package hackathon.mmeo;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;

public class FeelingActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_feeling);

        View.OnClickListener listenerFeeling = new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                Intent homePeintureIntent = new Intent(FeelingActivity.this, ParcoursPeinture.class);
                homePeintureIntent.putExtra("user", getIntent().getSerializableExtra("user"));
                startActivity(homePeintureIntent);
            }
        };

        findViewById(R.id.button_boring).setOnClickListener(listenerFeeling);
        findViewById(R.id.button_sad).setOnClickListener(listenerFeeling);
        findViewById(R.id.button_happy).setOnClickListener(listenerFeeling);
        findViewById(R.id.button_funny).setOnClickListener(listenerFeeling);
        findViewById(R.id.button_neutral).setOnClickListener(listenerFeeling);
    }
}
